if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    Vue.directive('init', {
        inserted: function(el) {
            pluginInit(el);
        },
        componentUpdated: function(el) {
            pluginInit(el);
        },
    });

    var vm = new Vue({
        el: '#app',
        data: {
            show: false,
            edm: [
                {
                    title: 'EDM 1. Memastikan Pengaturan dan Pemeliharaan Framework Tata Kelola',
                    subEdm: [
                        {
                            title: '1.1 Mengevaluasi sistem tata kelola. Secara terus menerus mengidentifikasi dan terlibat dengan stakeholder perusahaan, mencatat pemahaman kebutuhan. Dan membuat penilaian mengenai renacangan tata kelola IT Perusahaan saat ini dan masa depan',
                            aktivitas: [
                                {
                                    pertanyaan: '1. Apakah Sisfo Telkom menganalisa faktor-faktor internal dan external (hukum, regulasi, dan obligasi kontaktual) serta kecenderungan dalam lingkungan bisnis yang dapat mempengaruhi rancangan tatakelola?',
                                    rekomendasi: 'Meningkatkan evaluasi secara berkala terutama pada faktor-faktor internal dan external (hukum, regulasi, dan obligasi kontaktual) serta kecenderungan dalam lingkungan dengan membentuk sistem dan tim secara terintegrasi untuk menganalisa trend bisnis yang dapat mempengaruhi rancangan tata kelola.',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '2. Apakah Sisfo Telkom telah menetapkan kepentingan IT dan perannya berkenaan dengan kepentingan bisnis?',
                                    rekomendasi: 'Menetapkan sebuah kerangka kerja IT yang terintegrasi dengan tata kelola perusahaan.',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '3. Apakah Sisfo Telkom mempertimbangkan peraturan eksternal, hokum dan kewajiban kontrak dalam bagaimana penerapannya dengan tata kelola IT?',
                                    rekomendasi: 'Mengidentifikasi dan memahami lingkungan eksternal TI kemudian mengintegrasikannya dengan tata kelola IT.',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '4. Apakah Sisfo Telkom telah menyelasraskan etika penggunaan dan pengolahan informasi serta dampaknya pada masyarakat, lingkungan, dan minat stakeholder dengan arah, tujuan dan sasaran perusahaan?',
                                    rekomendasi: 'Melakukan manajemen audit independen untuk memeriksa dampak penggunaan dan pengolahan informasi terhadap masyarakat, lingkungan, dan minat stakeholder dengan arah, tujuan dan sasaran perusahaan secara terukur',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '5. Apakah Sisfo Telkom telah menetapkan implikasi dari kendali perusahaan secara keseluruhan terkait IT?',
                                    rekomendasi: 'Membuat mekanisme untuk mengintegrasikan tata kelola TI dengan kendali perusahaan.',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '6. Apakah Sisfo Telkom telah menyampaikan prinsip-prinsip sebagai panduan rancangan tata kelola dan pembuatan keputusan IT?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '7. Apakah Sisfo Telkom memahami budaya pembuatan keputusan perusahaan dan menetapkan model pembuat keputusan yang optimal untuk IT?',
                                    rekomendasi: 'Menetapkan model pembuat keputusan yang disesuaikan dengan budaya perusahaan.',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '8. Apakah Sisfo Telkom telah menetapkan level yang sesuai untuk pendelegasian wewenang termasuk aturan dasar untuk keputusan IT?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                            ]
                        },
                        {
                            title: '1.2 Mengarahkan sistem tata kelola. Menginformasikan pemimpin. Membimbing struktur, proses, dan praktek untuk tata kelola IT sejalan dengan prinsip rancangan tata kelola yang disetujui, model pembuat keputusan, dan level otoritas. Menetapkan informasi yang dibutuhkan untuk pembuat keputusan',
                            aktivitas: [
                                {
                                    pertanyaan: '1. Apakah Sisfo telah mengkomunikasikan prinsip tata kelola IT dan sepakat dengan manajemen eksekutif dalam rangka untuk membangun kepemimpinan yang terinformasi dan berkomitmen?',
                                    rekomendasi: 'Membangun komunikasi dan kesepakatan dengan manajemen eksekutif terkait prinsip tata kelola TI.',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '2. Apakah Sisfo telah membangun atau mendelegasikan pembuatan struktur, proses dan praktek tata kelola sejalan dengan prinsip rancangan yang disepakati?',
                                    rekomendasi: 'Menjalankan mekanisme proses tata kelola TI yang terintegrasi sesuai dengan prinsip rancangan yang disepakati dengan manajemen eksekutif.',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '3. Apakah Sisfo telah mengalokasikan tanggungjawab dan akuntabilitas sejalan dengan prinsip rancangan tata kelola yang disepakati, model pembuatan keputusan dan delegasi?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '4. Apakah Sisfo telah menjamin bahwa komunikasi dan mekanisme pelaporan memberikan tanggungjawab untuk pengawasan  dan pembuatan keputusan dengan informasi yang tepat?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '5. Apakah Sisfo telah mengarahkan staff mengikuti panduan etika dan perilaku professional dan menjamin bahwa konsekuensi dari ketidakpatuhan diketahui dan diberlakukan?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '6. Apakah Sisfo telah mengarahkan pembentukan sistem penghargaan untuk mendukung perubahan budaya yang diinginkan?',
                                    rekomendasi: 'Membuat sistem penghargaan TI yang terukur untuk mendukung perubahan budaya yang diinginkan.',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                            ]
                        },
                        {
                            title: '1.3 Mengawasi sistem tata kelola. Mengawasi keefektivan dan kinerja tata kelola IT. Melakukan penilaian apakah sistem tata kelola dan mekanisme yang diimplementasikan meliputi struktur, prinsip, dan proses berjalan secara efektif dan memberikan pengawasan IT yang tepat',
                            aktivitas: [
                                {
                                    pertanyaan: '1. Apakah Sisfo telah memberikan penilaian keefektifan dan kinerja stakeholder memberikan tanggungjawab dan wewenang yang didelegasikan untuk tata kelola IT?',
                                    rekomendasi: 'Meningkatkan pengawasan dengan membuat sistem audit untuk memeriksa kefektifan dan kinerja tata kelola TI sesuai tugasnya masing-masing.',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '2. Apakah Sisfo Telkom memberikan penilaian mengenai mekanisme IT yang disetujui (struktur, prinsip, proses dll) telah dibentuk dan berjalan dengan baik?',
                                    rekomendasi: 'Membuat prosedur penilaian TI yang terukur untuk memastikan mekanisme tata kelola TI berjalan sesuai prinsip yang disepakati.',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '3. Apakah Sisfo Telkom telah memberikan penilaian terhadap keefektifan rancangan tata kelola dan mengidentifikasi tindakan perbaikan untuk deviasi yang ditemukan?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '4. Apakah Sisfo Telkom meneruskan pengawasan lebih lanjut pada pemenuhan kewajiban IT, kebijakan internal, standar dan panduan professional?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '5. Apakah Sisfo Telkom memberikan pengawasan pada keefektifan, kepatuhan dan sistem control perusahaan?',
                                    rekomendasi: 'Membuat mekanisme dan prosedur pengawasan tata kelola TI.',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '6. Apakah Sisfo mengawasi mekanisme rutin untuk menjamin bahwa penggunaan IT sesuai dengan obligasi yang relevan, panduan dan standar?',
                                    rekomendasi: 'Menjalankan sistem pengawasan dan membuat pelaporan kinerja tata kelola TI secara berkala',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                            ]
                        },
                    ],
                },
                {
                    title: 'EDM 2. Memastikan Penyampaian Manfaat',
                    subEdm: [
                        {
                            title: '2.1 Mengevaluasi nilai optimasi. Terus mengevaluasi potofolio investasi IT, layanan dan asset untuk menentukan langkah dalam mencapai tujuan perusahaan dan menyampaikan nilai dengan biaya yang terjangkau. Mengidentifikasi dan membuat penilaian terhadap perubahan arah yang harus diberikan pada perusahaan untuk mengoptimalkan penciptaan nilai',
                            aktivitas: [
                                {
                                    pertanyaan: '1. Memahami kebutuhan stakeholder, isu IT stategis, seperti kebutuhan IT, wawasan dan kemampuan teknologi berkenaan dengan kepentingan IT yang aktual dan potensial untuk strategi perusahaan',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '2. Memahami elemen kunci dari tata kelola yang dibutuhkan untuk penggunaan layanan, asset dan sumber daya IT yang andal, aman dan biaya operasional yang efektif',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '3. Memahami dan melakukan diskusi secara rutin mengenai peluang yang dapat muncul dari perubahan perusahaan yang didukung oleh teknologi yang dimiliki atau teknologi baru dan mengoptimasi nilai yang diciptakan dari peluang tersebut',
                                    rekomendasi: 'Membentuk tim untuk menganalisa peluang dari perubahan perusahaan dan perkembangan teknologi untuk mengoptimasi nilai dari peluang tersebut dan menetapkan pertemuan dan diskusi secara berkala untuk mengoptimasi nilai',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '4. Memahami nilai yang ditegakan perusahaan dan menilai seberapa baik nilai tersebut dikomunikasikan, dipahami dan diaplikasikan melalui proses perusahaan',
                                    rekomendasi: 'Meninjau nilai yang ditegakan perusahaan dan membuat sistem penilaian untuk mengukur seberapa baik nilai perusahaan dikomunikasikan, dipahami dan diaplikasikan melalui proses perusahaan',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '5. Mengevaluasi seberapa efektif strategi IT dan strategi perusahaan terintegrasi dalam menciptakan nilai',
                                    rekomendasi: 'Membuat sistem evaluasi untuk mengukur fektivitas strategi IT dan strategi perusahaan terintegrasi dalam menciptakan nilai',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '6. Memahami dan menilai seberapa efektif peran, tanggunggunjawab dan akuntabilitas badan pembuat keputusan dalam menjamin penciptaan nilai dari investasi, layanan dan asset IT',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '7. Menilai seberapa baik manajemen investasi, layanan, dan asset IT tergabung dengan manajemen nilai dan keuangan perusahaan',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '8. Mengevaluasi potofolio investasi, layanan dan asset IT untukdigabungkan dengan strategi perusahaan',
                                    rekomendasi: 'Melakukan evaluasi secara berkala terhadap potofolio investasi, layanan dan asset IT untuk diintegrasikan dengan strategi perusahaan',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                            ]
                        },
                        {
                            title: '2.2 Mengarahkan optimasi nilai. Mengarahkan prinsip dan praktek manajemen nilai untuk memudahkan realisasi optimalisasi nilai dari investasi IT melalui seluruh siklus hidup ekonomi',
                            aktivitas: [
                                {
                                    pertanyaan: '1. Apakah Sisfo Telkom menentukan dan mengkomunikasikan potofolio dan jenis, kategori dan kriteria investasi?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '2. Apakah Sisfo telah menentukan syarat-syarat tahapan dan tinjauan lainnya untuk keperluan investasi untuk perusahaan, resiko yang berkaitan, jadwal program dan rencana pendanaan and penyampaian kunci kapabilitas dan manfaat serta kontribusi untuk menciptakan nilai?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '3. Apakah Sisfo telah mengarahkan manajemen untuk mempertimbangkan penggunaan IT yang inovatif sehingga memungkinkan perusahaan memperoleh peluang baru, bisnis baru, meningkatkan persingan dan mengembangkan proses?',
                                    rekomendasi: 'Mengadakan pertemuan rutin dengan manajemen untuk mendiskusikan inovasi-inovasi penggunaan TI untuk menciptakan peluang dan nilai baru bagi perusahaan.',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '4. Apakah Sisfo telah mengarahkan perubahan yang diperlukan dalam penugasan akuntabilitas dan tanggungjawab untuk melaksanakan portofolio investasi dan memberikan nilai dari proses dan layanan bisnis?',
                                    rekomendasi: 'Menetapkan tanggungjawab untuk melaksanakan portofolio investasi dan memberikan nilai dari proses dan layanan bisnis',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '5. Apakah Sisfo telah mengarahkan dan mengkomunikasikan tingkat nilai perusahaan dan ukuran hasil agara dapat diawasi secara efektif?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '6. Apakah Sisfo telah mengarahkan perubahan yang diperlukan pada portofolio investasi dan layanan untuk dihubungkan dengan tujuan yang diharapkan maupun hambatan-hambatan yang ada?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '7. Apakah Sisfo telah merekomendasikan pertimbangan inovasi yang potensial, penrubahan organisasi, atau pengembangan operasional yang dapat memberikan nilai bagi perusahaan?',
                                    rekomendasi: 'Melakukan riset dan pengembangan secara berkala untuk membuat rekomendasi inovasi yang dapat memberikan nilai bagi perusahaan',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                            ]
                        },
                        {
                            title: '2.3 Mengawasi nilai optimasi. Mengawasi tujuan kunci dan metrics untuk memutuskan sejauh mana bisnis dijalankan sesuai nilai yang diharapkan. Mengidentifikasi isu penting dan mempertimbangkan aksi korektif',
                            aktivitas: [
                                {
                                    pertanyaan: '1. Apakah Sisfo Telkom telah menetapkan proses tujuan kerja, metriks target dan patokan? Metriks harus meliputi aktivitas dan hasil tindakan termasuk menuntun indikator untuk hasil serta neraca keuangan keuangan dan nonfinasial. Meninjau dan menyetujui dengan IT dan fungsi bisnis lain juga stakeholder yang relevan?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '2. Apakah Sisfo Telkom telah mengumpulkan data yang relevan akurat, tepat waktu dan kredibel untuk melaporkan progress dalam menyampaikan nilai pada sasaran?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '3. Apakah Sisfo Telkom telah memperoleh potofolio tetap dan relevan, program dan laporan kinerja IT, meninjau tujuan perusahaan yang telah diidentifikasi dan sejauh mana rencana tujuan telah dicapai, target kinerja telah dipenuhi dan resiko telah ditangulangi?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '4. Apakah Sisfo Telkom telah mengambil tindakan manajemen yang sesuai dengan yang dibutuhkan untuk menjamin nilai telah dioptimasi?',
                                    rekomendasi: 'Melakukan evaluasi rutin dan mengambil tindakan manajemen yang sesuai dengan yang dibutuhkan untuk menjamin optimasi nilai',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '5. Apakah Sisfo Telkom telah menjamin tindakan korektif manajemen telah ditingkatkan dan dikontrol?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                            ]
                        },
                    ],
                },
                {
                    title: 'EDM 3. Memastikan Optimasi Resiko',
                    subEdm: [
                        {
                            title: '3.1 Mengevaluasi manajemen resiko. Secara terus menerus memeriksa dan membuat penilaian pada pengaruh resiko terhadap penggunaan IT di perusahaan pada saat ini dan masa depan',
                            aktivitas: [
                                {
                                    pertanyaan: '1. Apakah Sisfo Telkom telah menetapkan Tingkat resiko IT yang dapat diterima perusahaan untuk memenuhi tujuannya?',
                                    rekomendasi: 'Meningkatkan evaluasi pada manajemen resiko dengan membuat sistem penilaian tingkat resiko untuk mengidentifikasi dan menetapkan resiko IT yang dapat diterima perusahaan untuk memenuhi tujuannya',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '2. Apakah Sisfo Telkom telah mengevaluasi dan menyetujui toleransi resiko IT terhadap resiko yang dapat diterima perusahaan dan tingkat peluang?',
                                    rekomendasi: 'Melakukan evaluasi secara berkala untuk meninjau toleransi resiko TI terhadap resiko yang dapat diterima perusahaan dan tingkat peluangnya',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '3. Apakah Sisfo Telkom telah memutuskan tingkat keselarasan strategi resiko TI dengan resiko strategi perusahaan?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '4. Apakah Sisfo Telkom telah mengevaluasi secara proaktif faktor resiko IT terlebih dahulu dari keputusan strategi perushaan yang tertunda dan memastikan perusahaan telah membuat keputusan sadar resiko?',
                                    rekomendasi: 'Meninjau keputusan perusahaan dan mengidentifikasi resiko TI dari keputusan tersebut',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '5. Apakah Sisfo telah memutuskan bahwa penggunaan IT ditujukan untuk penilaian dan evaluasi resiko yang tepat sebagaimana digambarkan dalam standar internasional dan nasional yang relevan?',
                                    rekomendasi: 'Meningkatkan penguasaan Teknologi Informasi dengan merekrut pengelola berlatar belakang IT untuk memastikan bahwa sistem penilaian dan evaluasi resiko yang dijalankan tepat dan sesuai dengan standar internasional',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '6. Mengevaluasi aktivitas manajemen resiko untuk memastikan keselarasan dengan kapasitas perusahaan untuk kerugian IT dan toleransi kepemimpinan untuk hal tersebut?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                            ]
                        },
                        {
                            title: '3.2 Mengarahkan manajemen resiko. Resiko harus dikelola sesuai dengan kebijakan, prosedur,yang dipublikasikan dan dieskalasikan dengan pembuat keputusan yang relevan.',
                            aktivitas: [
                                {
                                    pertanyaan: '1. Apakah sisfo telah menggalakan budaya sadar resiko IT dan memberi kuasa pada perusahaan untuk mengidentifikasi resiko IT, peluang dan dampak bisnis yang potensial secara proaktif?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '2. Apakah sisfo telah mengarahkan integrasi strategi dan operasi resiko IT dengan keputusan strategi dan operasi resiko perusahaan? ',
                                    rekomendasi: 'Mengadakan pertemuan rutin dan memastikan Sisfo telah menintegrasikan strategi dan operasi resiko IT dengan keputusan strategi dan operasi resiko perusahaan',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '3. Apakah Sisfo telah mengarahkan rencana komunikasi resiko dan juga recana tindakan resiko?',
                                    rekomendasi: 'Menetapkan kerangka kerja komunikasi dan tindakan resiko secara terintegrasi dan terukur',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '4. Apakah sisfo telah mengarahkan implementasi dari mekanisme yang sesuai untuk merespon secara cepat untuk mengubah resiko dan melaporkan secara cepat untuk level manajemen yang sesuai didukung oleh prinsip eskalasi yang disetujui?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '5. Apakah sisfo telah mengarahkan resiko, peluang dan isu dan kepentingan dapat diidentifikasi dan dilaporkan oleh setiap orang setiap saat?',
                                    rekomendasi: 'Membuat mekanisme identifikasi, pelaporan dan pengelolaan resiko sesuai dengan kebijakan dan prosedur yang ditetapkan',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '6. Apakah sisfo telah mengidentifikasi tujuan kunci dan metric resiko dari tata kelola resiko dan proses manajemen untuk dapat dimonitor, dan menyetujui pendekatan, metode teknik dan proses untuk menangkap dan melaporkan informasi pengukuran?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                            ]
                        },
                        {
                            title: '3.3 Mengawasi nilai optimasi. Mengawasi tujuan kunci dan metrics untuk memutuskan sejauh mana bisnis dijalankan sesuai nilai yang diharapkan. Mengidentifikasi isu penting dan mempertimbangkan aksi korektif',
                            aktivitas: [
                                {
                                    pertanyaan: '1. Apakah Sisfo Telkom telah mengawasi sejauh mana profil resiko dikelola dalam ambang batas resiko?',
                                    rekomendasi: 'Melakukan pemeriksaan rutin serta pengendalian terhadap pengelolaan resiko agar resiko yang dikelola tidak melewati ambang batas resiko',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '2. Apakah Sisfo Telkom telah mengawasi tujuan kunci dan metric proses tata kelola resiko terhadap target, menganalisa penyebab deviasi dan menginisiasi tindakan perbaikan untuk penyebab utama?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '3. Apakah Sisfo Telkom mengaktifkan ulasan kunci stakeholder mengenai perkembangan perusahaan pada tujuan yang teridentifikasi?',
                                    rekomendasi: 'Mengidentifikasi persepsi stakeholder mengenai perkembangan perusahaan dengan mengaktifkan ulasan stakeholder',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '4. Apakah Sisfo Telkom telah melaporkan isu tata kelola resiko bada badan komite eksekutif?',
                                    rekomendasi: 'Membuat pelaporan isu tata kelola resiko terhadap badan komite eksekutif secara rutin',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                            ]
                        },
                    ],
                },
                {
                    title: 'EDM 4. Memastikan Optimasi Sumber Daya',
                    subEdm: [
                        {
                            title: '4.1 Mengevaluasi manajemen sumberdaya. Secara terus menerus memeriksa dan membuat penilaian terhadap sumberdaya IT, pilihan untuk resourcing, alokasi dan prinsip manajemen yang sesuai dengan kebutuhan perusahaan untuk saat ini dan masa yang akan datang agar menjadi optimal',
                            aktivitas: [
                                {
                                    pertanyaan: '1. Apakah Sisfo Telkom telah memeriksa dan membuat penilaian pada strategi optimasi sumber data saat ini dan masa depan, pilihan untuk menyediakan sumber daya TI, dan mengembangkan kemampuan untuk memenuhi kebutuhan saat ini dan kebutuhan masa depan (termasuk opsi sourcing)?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '2. Apakah Sisfo Telkom telah menentukan prinsip-prinsip untuk mengarahkan alokasi dan pengelolaan sumber daya dan kemampuan sehingga TI dapat memenuhi kebutuhan perusahaan, dengan kemampuan yang dibutuhkan dan kapasitas sesuai dengan yang disepakati pada prioritas dan keterbatasan anggaran?',
                                    rekomendasi: 'Membuat kerangka kerja alokasi dan pengelolaan sumber daya TI agar sesuai dengan prioritas dan anggaran perusahaan',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '3. Apakah Sisfo Telkom telah meninjau dan menyetujui rencana sumber daya dan strategi arsitektur perusahaan untuk memberikan nilai dan mitigasi risiko dengan sumber daya yang dialokasikan?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '4. Apakah Sisfo Telkom telah memahami persyaratan untuk menyelaraskan pengelolaan sumber daya dengan sumber daya keuangan dan perencanaan SDM perusahaan?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '5. Apakah Sisfo Telkom telah menentukan prinsip pengelolaan dan pengendalian dari arsitektur perusahaan?',
                                    rekomendasi: 'Menetapkan prinsip pengelolaan dan pengendalian dari arsitektur perusahaan',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                            ]
                        },
                        {
                            title: '4.2 Mengarahkan manajemen sumber daya. Memastikan prinsip tata kelola adopsi sumber daya untuk memungkinkan penggunaan sumber daya IT yang optimal sepanjang siklus hidup ekonomi.',
                            aktivitas: [
                                {
                                    pertanyaan: '1. Apakah Sisfo Telkom telah mengkomunikasikan dan mendorong penerapan strategi pengelolaan sumber daya, prinsip, dan setuju akan rencana sumber daya dan strategi arsitektur perusahaan?',
                                    rekomendasi: 'Mengadakan pertemuan rutin untuk mendiskusikan strategi, prinsip dan rencana sumber daya untuk diintegrasikan dengan arsitektur perusahaan',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '2. Apakah Sisfo Telkom telah menetapkan tanggung jawab untuk melaksanakan pengelolaan sumber daya?',
                                    rekomendasi: 'Membuat tim yang secara khusus bertanggungjawab melaksanakan pengelolaan sumber daya',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '3. Apakah Sisfo Telkom telah menentukan tujuan utama, pengukuran dan metrik untuk pengelolaan sumber daya?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '4. Apakah Sisfo Telkom telah menetapkan prinsip-prinsip yang berkaitan dengan menjaga sumber daya?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '5. Apakah Sisfo Telkom telah menyelaraskan pengelolaan sumber daya dengan keuangan dan perencanaan SDM perusahaan?',
                                    rekomendasi: 'Meningkatkan pengelolaan sumber daya terutama sumberdaya manusia yang menguasai TIK untuk memastikan bahwa pengelolaan sumber daya sesuai dengan dengan keuangan dan perencanaan SDM perusahaan',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                            ]
                        },
                        {
                            title: '4.3 Mengawasi manajemen sumber daya. Mengawasi tujuan kunci dan metric proses manajemen sumberdaya  dan menyusun cara agar deviasi atau masalah dapat diidentifikasi, dilacak dan dilaporkan untuk perbaikan.',
                            aktivitas: [
                                {
                                    pertanyaan: '1. Apakah Sisfo Telkom telah memantau alokasi dan optimalisasi sumber daya sesuai dengan tujuan perusahaan dan prioritas menggunakan disetujui tujuan dan metric?',
                                    rekomendasi: 'Melakukan evaluasi rutin dan membuat metric penilaian untuk memantau alokasi dan optimalisasi sumber daya sesuai dengan tujuan perusahaan dan prioritas',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '2. Apakah Sisfo Telkom telah memonitor strategi sourcing TI, strategi arsitektur perusahaan, sumber daya TI dan kemampuan untuk memastikan bahwa kebutuhan saat ini dan masa depan perusahaan dapat dipenuhi?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '3. Apakah Sisfo Telkom telah memantau kinerja sumber daya, menganalisa penyebab penyimpangan, dan memulai tindakan perbaikan untuk mengatasi penyebab yang mendasari?',
                                    rekomendasi: 'Meningkatkan pengawasan pada manajemen sumber daya dengan membuat pengukuran kinerja untuk mengawasi kinerja sumber daya dan mengidentifikasi permasalahan dan faktor-faktor penyebab penyimpangan kemudian melakukan tindakan perbaikan secara cepat',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                            ]
                        },
                    ],
                },
                {
                    title: 'EDM 5. Memastikan Transparansi Stakeholder',
                    subEdm: [
                        {
                            title: '5.1 Secara terus-menerus memeriksa dan membuat penilaian akan kebutuhan saat ini dan yang akan datang untuk komunikasi dan pelaporan stakeholder, termasuk kebutuhan pelaporan wajib (seperti peraturan) dan komunikasi ke stakeholder lainnya. Menetapkan prinsip untuk komunikasi',
                            aktivitas: [
                                {
                                    pertanyaan: '1. Apakah Sisfo Telkom telah memeriksa dan membuat keputusan mengenai persyaratan pelaporan wajib saat ini dan masa depan yang berkaitan dengan penggunaan TI dalam perusahaan (peraturan, undang-undang, hukum umum, kontrak), termasuk tingkat dan frekuensi?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '2. Apakah Sisfo Telkom telah memeriksa dan membuat keputusan pada persyaratan pelaporan saat ini dan masa depan bagi para stakeholder lain yang berkaitan dengan penggunaan TI dalam perusahaan, termasuk tingkat dan kondisi?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '3. Apakah Sisfo Telkom telah menjaga prinsip komunikasi dengan stakeholder eksternal dan internal, termasuk format dan saluran komunikasi, stakeholder acceptance dan sign-off dari pelaporan?',
                                    rekomendasi: 'Meningkatkan transparansi antara lembaga dan stakeholder dengan membuat mekanisme komunikasi dengan stakeholder eksternal dan internal yang meliputi format dan saluran komunikasi, stakeholder acceptance dan sign-off dari pelaporan. Selain itu harus dilakukan pertemuan dan rapat koordinasi secara rutin',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                            ]
                        },
                        {
                            title: '5.2 Memastikan pembentukan komunikasi dan pelaporan stakeholder yang efektif, termasuk mekanisme untuk memastikan kualitas dankelengkapan informasi, kelalaian dari pelaporan wajib, dan menciptakan strategi komunikasi untuk para stakeholder.',
                            aktivitas: [
                                {
                                    pertanyaan: '1. Apakah Sisfo Telkom telah membentuk strategi komunikasi untuk stakeholder internal maupun eksternal?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '2. Apakah Sisfo Telkom telah mengarahkan mekanisme pelaksanaan untuk memastikan bahwa informasi memenuhi semua kriteria untuk persyaratan pelaporan wajib TI untuk perusahaan?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '3. Apakah Sisfo Telkom telah Membangun mekanisme untuk validasi dan persetujuan pelaporan wajib?',
                                    rekomendasi: 'Membangun dan menjalankan mekanisme validasi dan prosedur pelaporan secara terintegrasi dan terukur',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '4. Apakah Sisfo Telkom telah Menetapkan mekanisme pelaporan eskalasi?',
                                    rekomendasi: 'Menetapkan mekanisme pelaporan eskalasi agar dapat terukur dan terintegrasi',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                            ]
                        },
                        {
                            title: '5.3 Memonitor keefektivan komunikasi stakeholder. Mekanisme penilaian untuk memastikan akurasi, keandalan dan efektivitas, serta memastikan apakah kebutuhan dari stakeholder yang berbeda telah sesuai.',
                            aktivitas: [
                                {
                                    pertanyaan: '1. Apakah Sisfo Telkom telah melakukan penilaian efektivitas mekanisme untuk memastikan keakuratan dan keandalan pelaporan wajib secara berkala?',
                                    rekomendasi: 'Meningkatkan analisa',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '2. Apakah Sisfo Telkom telah  melakukan penilaian secara berkala mengenai efektivitas mekanisme, dan hasil dari, komunikasi dengan stakeholder eksternal dan internal?',
                                    rekomendasi: 'Membuat prosedur mekanisme penilaian untuk dapat melakukan penilaian secara berkala mengenai efektivitas mekanisme, dan hasil dari, komunikasi dengan stakeholder eksternal dan internal',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                                {
                                    pertanyaan: '3. Apakah Sisfo Telkom telah   melakukan penilaian secara berkala mengenai apakah requirement stakeholder terpenuhi?',
                                    rekomendasi: 'Menjalankan penilaian rutin terhadap pemenuhan requirement stakeholder dengan melakukan pemeriksaan rutin terhadap sistem yang dijalankan, menyediakan fasilitas pelaporan bagi stakeholder, serta melakukan analisis kepuasan stakeholder kemudian mencatat kinerja dari pelaporan secara rinci dan merinci detail detail tindakan yang harus dilakukan apabila kinerja tidak tercapai',
                                    jawabanSurvey: null,
                                    jawabanTarget: null,
                                },
                            ]
                        },
                    ],
                },
            ],
        },
        methods: {},
    });
}